import React from "react";
import { BrowserRouter } from "react-router-dom";

import Header from "./components/Header";
import AnimRoutes from "./components/AnimRoutes";

const App = () => {
  return (
    <>
      <BrowserRouter>
        <Header />
        <AnimRoutes />
      </BrowserRouter>
    </>
  );
};

export default App;
