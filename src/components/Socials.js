import { ImPinterest, ImInstagram, ImYoutube } from "react-icons/im";

const Socials = () => {
  return (
    <div className="hidden xl:flex ml-24">
      <ul className="flex gap-x-4">
        <li className="cursor-pointer">
          <ImPinterest />
        </li>
        <li className="cursor-pointer">
          <ImInstagram />
        </li>
        <li className="cursor-pointer">
          <ImYoutube />
        </li>
      </ul>
    </div>
  );
};

export default Socials;
